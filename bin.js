#!/usr/bin/env node

/** @see docs.npmjs.com/files/package.json#bin */

//----------------------------------------------------------------------//
//                            ES strict mode                            //
//----------------------------------------------------------------------//

'use strict';


//----------------------------------------------------------------------//
//                       Imported Node.js modules                       //
//----------------------------------------------------------------------//

const fs = require('fs');  // nodejs.org/api/fs.html


//----------------------------------------------------------------------//
//                         Imported NPM modules                         //
//----------------------------------------------------------------------//

const commandLineArgs   = require('command-line-args');    // www.npmjs.com/package/command-line-args
const stripJsonComments = require('strip-json-comments');  // www.npmjs.com/package/strip-json-comments


//----------------------------------------------------------------------//
//                        Imported local modules                        //
//----------------------------------------------------------------------//

const dESLintRunner = require('./main.js');


//----------------------------------------------------------------------//
//                              Constants                               //
//----------------------------------------------------------------------//

/**
 * "If no value was set, you will receive null."
 *
 * @see github.com/75lb/command-line-args/tree/v4.0.7#optiontype--function
 */
const OPTION_SUPPLIED_WITHOUT_VALUE = null;

/** @see github.com/75lb/command-line-args/tree/v4.0.7#optiondefinition- */
const ARGS_CONFIG = [
    { name: 'configFile',     type: String                       },
    { name: 'execute',        type: Boolean, defaultValue: false },
    { name: 'watch',          type: Boolean, defaultValue: false },
    { name: 'autofixIssues',  type: Boolean, defaultValue: false },
    { name: 'failOnWarnings', type: Boolean, defaultValue: false },
    { name: 'files',          type: String,  multiple: true, defaultOption: true }
];

/** @see nodejs.org/dist/v8.9.0/docs/api/fs.html#fs_fs_readfilesync_path_options */
const CONFIG_FILE_ENCODING = 'utf8';


//----------------------------------------------------------------------//
//                           Helper functions                           //
//----------------------------------------------------------------------//

/** @throws {Error} */
function validateOption(option, value) {

    if (typeof value === 'undefined') {  // Option was not supplied on the command line.
        throw new Error(option + ' option is required');
    }

    if (value === OPTION_SUPPLIED_WITHOUT_VALUE) {  // Option was supplied, but without a value.
        throw new Error(option + ' option must be supplied with a value');
    }

}

function setExitErrorCode() {

    /** @see nodejs.org/dist/v8.9.0/docs/api/process.html#process_process_exitcode */
    process.exitCode = 1;

}


//----------------------------------------------------------------------//
//                            Main function                             //
//----------------------------------------------------------------------//

function main() {

    try {

        /**
         * @see    github.com/75lb/command-line-args/tree/v4.0.7#api-reference
         * @throws {Error}
         */
        const parsedArgs = commandLineArgs(ARGS_CONFIG);

        /** @throws {Error} */
        validateOption('--configFile', parsedArgs.configFile);

        /**
         * @see    nodejs.org/dist/v8.9.0/docs/api/fs.html#fs_fs_readfilesync_path_options
         * @throws {Error}
         */
        const configFileJson = fs.readFileSync(parsedArgs.configFile, CONFIG_FILE_ENCODING);

        /** @see github.com/sindresorhus/strip-json-comments/tree/v2.0.1#usage */
        const configFileJsonStripped = stripJsonComments(configFileJson);

        /** @throws {Error} */
        const configs = JSON.parse(configFileJsonStripped);

        if (parsedArgs.execute) {

            dESLintRunner.execute(configs, parsedArgs.autofixIssues, parsedArgs.failOnWarnings, parsedArgs.files)
                .catch(setExitErrorCode)
                .then(function () {
                    if (parsedArgs.watch) {
                        dESLintRunner.watch(configs, parsedArgs.autofixIssues, parsedArgs.files);
                    }
                });

        } else if (parsedArgs.watch) {

            dESLintRunner.watch(configs, parsedArgs.autofixIssues, parsedArgs.files);

        } else {

            throw new Error('Must specify at least one of: --execute, --watch');

        }

    } catch (e) {

        console.log('ERROR: ' + e.message);

        setExitErrorCode();

    }

}

main();
