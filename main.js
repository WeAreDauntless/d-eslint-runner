//----------------------------------------------------------------------//
//                            ES strict mode                            //
//----------------------------------------------------------------------//

'use strict';


//----------------------------------------------------------------------//
//                         Imported NPM modules                         //
//----------------------------------------------------------------------//

const chalk     = require('chalk');             // www.npmjs.com/package/chalk
const chokidar  = require('chokidar');          // www.npmjs.com/package/chokidar
const ESLint    = require('eslint').CLIEngine;  // www.npmjs.com/package/eslint
const minimatch = require('minimatch');         // www.npmjs.com/package/minimatch


//----------------------------------------------------------------------//
//                           Helper functions                           //
//----------------------------------------------------------------------//

function multiline(lines) {

    return lines.join('\n');

}

function matchFilesAgainstGlobs(filepaths, globs) {

    return filepaths.filter(function (filepath) {
        return globs.some(function (glob) {
            return minimatch(filepath, glob);  // github.com/isaacs/minimatch/tree/v3.0.4#usage
        });
    });

}

function launchWatcher(runner, globs) {

    console.log(chalk.blue(multiline([
        'Watching ' + runner.configName + ' files:',
        ...globs.map((glob) => '    ' + glob),
        ''
    ])));

    chokidar.watch(globs)  // github.com/paulmillr/chokidar/tree/2.0.0#api
        .on('change', function (pathChangedFile) {

            // nodejs.org/dist/v8.9.0/docs/api/tty.html#tty_writestream_columns
            console.log('-'.repeat(process.stdout.columns));

            runner.run([pathChangedFile])
                .then((output) => console.log(output))
                .catch((err) => console.log('ERROR: ' + err));

        })
        .on('error', (err) => console.log('ERROR: ' + err));

}


//----------------------------------------------------------------------//
//                               Classes                                //
//----------------------------------------------------------------------//

class ESLintRunner {

    constructor(configName, configFile, autofixIssues, failOnWarnings, engineExtraOptions = {}) {

        this.configName     = configName;
        this.configFile     = configFile;
        this.autofixIssues  = autofixIssues;
        this.failOnWarnings = failOnWarnings;

        this.foundProblems = null;

        this.engine = new ESLint(Object.assign(  // eslint.org/docs/developer-guide/nodejs-api#cliengine
            {},
            engineExtraOptions,
            {
                configFile,
                fix: Boolean(autofixIssues)
            }
        ));

        this.formatter = this.engine.getFormatter(); // eslint.org/docs/developer-guide/nodejs-api#clienginegetformatter

    }

    run(globs) {

        console.log('Running ESLint (' + this.configName + ' config' + (this.autofixIssues ? ', autofix' : '') + ')');

        const colouredGlobs = globs.map((glob) => chalk.gray(glob));

        return new Promise((resolve) => {

            // eslint.org/docs/developer-guide/nodejs-api#cliengineexecuteonfiles
            const eslintReport = this.engine.executeOnFiles(globs);

            this.foundProblems = (
                eslintReport.errorCount > 0 ||
                (this.failOnWarnings && eslintReport.warningCount > 0)
            );

            ESLint.outputFixes(eslintReport);  // eslint.org/docs/developer-guide/nodejs-api#cliengineoutputfixes

            if (eslintReport.errorCount + eslintReport.warningCount > 0) {
                resolve(multiline([
                    ...colouredGlobs,
                    this.formatter(eslintReport.results)
                ]));
            } else {
                resolve(multiline([
                    ...colouredGlobs,
                    chalk.green('No problems!'),
                    ''
                ]));
            }

        });

    }

    didFindProblems() {

        return this.foundProblems;

    }

}


//----------------------------------------------------------------------//
//                            Main functions                            //
//----------------------------------------------------------------------//

function createRunnersFromConfigs(configs, autofixIssues, failOnWarnings = false) {

    const runners = {};

    Object.keys(configs).forEach(function (key) {
        const config = configs[key];
        runners[key] = new ESLintRunner(key, config.configFile, autofixIssues, failOnWarnings, config.engineOptions);
    });

    return runners;

}

function execute(configs, autofixIssues, failOnWarnings, filepaths = []) {

    const runners = createRunnersFromConfigs(configs, autofixIssues, failOnWarnings);

    const promises = Object.keys(configs).map(function (key) {
        const runner = runners[key];
        const globs  = configs[key].globs;
        if (filepaths.length === 0) {
            return runner.run(globs);  // No custom filepaths, so run using configured globs.
        }
        const matchedFilepaths = matchFilesAgainstGlobs(filepaths, globs);
        if (matchedFilepaths.length === 0) {
            return Promise.resolve();  // Nothing to do (no matching filepaths for this runner).
        }
        return runner.run(matchedFilepaths);
    });

    return Promise.all(promises)
        .then(function (outputs) {

            outputs.forEach(function (output) {
                if (output) {
                    console.log(output);
                }
            });

            const didFindProblems = Object.keys(runners)
                .map((key) => runners[key])
                .some((runner) => runner.didFindProblems());

            if (didFindProblems) {
                return Promise.reject(new Error());
            } else {
                return Promise.resolve();
            }

        });

}

function watch(configs, autofixIssues, filepaths = []) {

    const runners = createRunnersFromConfigs(configs, autofixIssues);

    Object.keys(configs).forEach(function (key) {
        const runner = runners[key];
        const globs  = configs[key].globs;
        if (filepaths.length === 0) {
            launchWatcher(runner, globs);  // No custom filepaths, so launch using configured globs.
            return;
        }
        const matchedFilepaths = matchFilesAgainstGlobs(filepaths, globs);
        if (matchedFilepaths.length === 0) {
            return;  // Nothing to do (no matching filepaths for this watcher).
        }
        launchWatcher(runner, matchedFilepaths);
    });

}

module.exports = {  // nodejs.org/dist/v8.9.0/docs/api/modules.html#modules_module_exports
    execute,
    watch
};
